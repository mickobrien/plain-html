var avantgarde = {
  src: 'http://lukas.xcomms.ie/meteor/templates/swf/avant-garde.swf'
};

sIFR.activate(avantgarde);

ratioArr = [8, 1.41, 9, 1.33, 12, 1.35, 18, 1.3, 20, 1.26, 27, 1.27, 28, 1.25, 32, 1.26, 33, 1.24, 38, 1.25, 53, 1.24, 56, 1.23, 57, 1.24, 84, 1.23, 85, 1.22, 87, 1.23, 89, 1.22, 91, 1.23, 94, 1.22, 95, 1.23, 103, 1.22, 104, 1.23, 1.22];

sIFR.replace(avantgarde, {
  selector: '.insidehome h1',
	css: ['.sIFR-root { color:#ff4901; display:inline; margin:0;}'
	,'a { cursor:pointer }'  
      ,'a:link { color:#ff4901; text-decoration:underline }'
      ,'a:hover { color:#ff4901; text-decoration:none }'
    ], 
	wmode : 'transparent',
	ratios : ratioArr,
	tuneHeight : '-5'
});

sIFR.replace(avantgarde, {
  selector: '.inside h1',
	css: ['.sIFR-root { color:#ff4901; display:inline; margin:0;}'
	,'a { cursor:pointer }'  
      ,'a:link { color:#ff4901; text-decoration:underline }'
      ,'a:hover { color:#ff4901; text-decoration:none }'
    ],  
	wmode : 'transparent',
	ratios : ratioArr,
	tuneHeight : '-5'
});

sIFR.replace(avantgarde, {
  selector: '.insidehome h2',
	css: ['.sIFR-root { color:#ff4901; font-weight:bold; display:inline; margin:0;}'
	,'a { cursor:pointer }'  
      ,'a:link { color:#ff4901; text-decoration:underline }'
      ,'a:hover { color:#ff4901; text-decoration:none }'
    ],
	wmode : 'transparent',
	ratios : ratioArr,
	tuneHeight : '-5'
});

sIFR.replace(avantgarde, {
  selector: '.inside h2',
	css: ['.sIFR-root { color:#ff4901; font-weight:bold; display:inline; margin:0}'
	,'a { cursor:pointer }'  
      ,'a:link { color:#ff4901; text-decoration:underline }'
      ,'a:hover { color:#ff4901; text-decoration:none }'
    ],
	wmode : 'transparent',
	ratios : ratioArr,
	tuneHeight : '-5'
});

sIFR.replace(avantgarde, {
  selector: '.insidehome h3',
	css: ['.sIFR-root { color:#4d4d4d; font-weight:normal; }'
	,'a { cursor:pointer }'  
      ,'a:link { color:#4d4d4d; text-decoration:underline }'
      ,'a:hover { color:#4d4d4d; text-decoration:none }'
    ],
	wmode : 'transparent',
	ratios : ratioArr,
	tuneHeight : '-5'
});

sIFR.replace(avantgarde, {
  selector: '.inside h3',
	css: ['.sIFR-root { color:#4d4d4d; font-weight:normal; }'
	,'a { cursor:pointer }'  
      ,'a:link { color:#4d4d4d; text-decoration:underline }'
      ,'a:hover { color:#4d4d4d; text-decoration:none }'
    ],
	wmode : 'transparent',
	ratios : ratioArr,
	tuneHeight : '-5'
});


sIFR.replace(avantgarde, {
  selector: '.video h1',
	css: ['.sIFR-root { color:#ff4901; font-weight:normal; }'
	,'a { cursor:pointer }'  
      ,'a:link { color:#ff4901; text-decoration:underline }'
      ,'a:hover { color:#ff4901; text-decoration:none }'
    ],
	wmode : 'transparent',
	ratios : ratioArr
});

sIFR.replace(avantgarde, {
  selector: '.video h2',
	css: ['.sIFR-root { color:#4d4d4d; display:inline; font-weight:normal; }'
	,'a { cursor:pointer }'  
      ,'a:link { color:#4d4d4d; text-decoration:underline }'
      ,'a:hover { color:#4d4d4d; text-decoration:none }'
	  ,'em { color:#a7a7a7; font-style:normal; }'
    ],
	wmode : 'transparent',
	ratios : ratioArr
});

sIFR.replace(avantgarde, {
  selector: '.events h1',
	css: ['.sIFR-root { color:#ff4901; display:inline; margin:0}'
	,'a { cursor:pointer }'  
      ,'a:link { color:#ff4901; text-decoration:underline }'
      ,'a:hover { color:#ff4901; text-decoration:none }'
    ],  
	wmode : 'transparent',
	ratios : ratioArr,
	tuneHeight : '-5'
});

sIFR.replace(avantgarde, {
  selector: '.events h2',
	css: ['.sIFR-root { color:#ff4901; font-weight:bold; display:inline; margin:0}'
	,'a { cursor:pointer }'  
      ,'a:link { color:#ff4901; text-decoration:underline }'
      ,'a:hover { color:#ff4901; text-decoration:none }'
    ],
	wmode : 'transparent',
	ratios : ratioArr,
	tuneHeight : '-5'
});

sIFR.replace(avantgarde, {
  selector: '.events h3',
	css: ['.sIFR-root { color:#4d4d4d; font-weight:normal; }'
	,'a { cursor:pointer }'  
      ,'a:link { color:#4d4d4d; text-decoration:underline }'
      ,'a:hover { color:#4d4d4d; text-decoration:none }'
    ],
	wmode : 'transparent',
	ratios : ratioArr,
	tuneHeight : '-5',
	fitExactly: true
});