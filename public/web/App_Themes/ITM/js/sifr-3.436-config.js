sIFR.fixWrap 	= true;
sIFR.fixHover  	= true;
sIFR.forceClear  = true;
sIFR.useDomLoaded = false;

var helvetica = {
	src:strThemeDir + 'swf/helvetica-neue.swf'
	//ratios:[6, 1.51, 8, 1.4, 10, 1.27, 11, 1.22, 12, 1.2, 18, 1.19, 21, 1.15, 28, 1.16, 29, 1.14, 30, 1.15, 40, 1.14, 52, 1.13, 58, 1.12, 59, 1.13, 93, 1.12, 97, 1.11, 99, 1.12, 107, 1.11, 108, 1.12, 1.11]
	};
	
var palatino = {
	src:strThemeDir + 'swf/palatino-italic.swf'
	//ratios:[6, 1.51, 8, 1.4, 10, 1.27, 11, 1.22, 12, 1.2, 18, 1.19, 21, 1.15, 28, 1.16, 29, 1.14, 30, 1.15, 40, 1.14, 52, 1.13, 58, 1.12, 59, 1.13, 93, 1.12, 97, 1.11, 99, 1.12, 107, 1.11, 108, 1.12, 1.11]
	}; 

sIFR.activate(palatino, helvetica );
	
	/* Sections */

	sIFR.replace(helvetica, {
		selector:'.header-date',
		css:['.sIFR-root 	{ background-color:#ffffff; color:#8d8b8a; }',
				 'strong 			{ background-color:#ffffff; color:#8d8b8a; font-weight:bold; }'
			  ]
		,wmode:'transparent'
	});
	
	sIFR.replace(helvetica, {
		selector:'.show-info-primary h1, .show-info-feature h1',
		css:['.sIFR-root 	{ background-color:#1a1715; color:#ffffff; font-weight:bold; leading:-3; }',
			  'a 							{ background-color:#1a1715; color:#ffffff; text-decoration:none; } ',
			  'a:hover 				{ background-color:#1a1715; color:#ffffff; text-decoration:underline;} '		  
			  ]
		,wmode:'transparent'
		,tuneHeight: -7
		,offsetTop : -3
	});
	
	sIFR.replace(helvetica, {
		selector:'.show-info h1',
		css:['.sIFR-root 	{ background-color:#1a1715; color:#ffffff; font-weight:bold; leading:-7; }',
			  'a 							{ background-color:#1a1715; color:#ffffff; text-decoration:none; } ',
			  'a:hover 				{ background-color:#1a1715; color:#ffffff; text-decoration:underline;} '		  
			  ]
		,wmode:'transparent'
		,tuneHeight: -7
		,offsetTop : -3
	});
	
	sIFR.replace(helvetica, {
		selector:'.show-info-home h2, .show-info-home h4, .show-info h4',
		css:['.sIFR-root 	{ background-color:#1a1715; color:#c6c5c4; font-weight:normal; }',
			  'a 							{ background-color:#1a1715; color:#c6c5c4; text-decoration:none; } ',
			  'a:hover 				{ background-color:#1a1715; color:#c6c5c4; text-decoration:underline;} '		  
			  ]
		,wmode:'transparent'
		,tuneHeight: -7
		,offsetTop : -3
	});
	
	sIFR.replace(helvetica, {
		selector:'.show-info h2, .show-info h3',
		css:['.sIFR-root 	{ background-color:#1a1715; color:#ffffff; font-weight:normal; }',
			  'a 							{ background-color:#1a1715; color:#ffffff; text-decoration:none; } ',
			  'a:hover 				{ background-color:#1a1715; color:#ffffff; text-decoration:underline;} '		  
			  ]
		,wmode:'transparent'
		,tuneHeight: -7
		,offsetTop : -3
	});
	
		
	
	/* Content */
	
	sIFR.replace(helvetica, {
		selector:'.content .panels h1',
		css:['.sIFR-root 	{ color:#4f658c; }',
			  'a 							{ background-color:#ffffff; color:#4f658c; text-decoration:none; } ',
			  'a:hover 				{ background-color:#ffffff; color:#1a1715; } '		  
			  ]
		,wmode:'transparent'
	});
	
	sIFR.replace(helvetica, {
		selector:'.review h1',
		css:['.sIFR-root 	{ background-color:#ffffff; color:#535150; }',
			  'strong 				{ background-color:#ffffff; color:#4f658c; font-weight:bold; } ',
				'strong a				{ background-color:#ffffff; color:#4f658c; font-weight:bold; } ',
			  'a 							{ background-color:#ffffff; color:#535150; text-decoration:none; } ',
			  'a:hover 				{ background-color:#ffffff; color:#535150; text-decoration:underline; } '		  
			  ]		
	});
	
	sIFR.replace(helvetica, {
		selector:'.news h1',
		css:['.sIFR-root 	{ background-color:#ffffff; color:#4f658c; }',
			  'a 							{ background-color:#ffffff; color:#4f658c; text-decoration:none; } ',
			  'a:hover 				{ background-color:#ffffff; color:#4f658c; text-decoration:underline; } '		  
			  ]		
	});
	
	
	sIFR.replace(helvetica, {
		selector:'.row-img-scroll-feature h1',
		css:['.sIFR-root 	{ background-color:#1a1715; color:#ffffff; font-weight:bold; leading:-10; }',
			  'a 							{ background-color:#1a1715; color:#ffffff; text-decoration:none; } ',
			  'a:hover 				{ background-color:#1a1715; color:#ffffff; text-decoration:underline;} '		  
			  ]
		,wmode:'transparent'
		,tuneHeight: -7
		,offsetTop : -3
		,filters: {
        DropShadow: {
           distance: 1
          ,color: '#000000'
          ,strength: 1
          ,alpha: .25
        }
      }
	});
	
	sIFR.replace(helvetica, {
		selector:'.content h1, .line-top h1',
		css:['.sIFR-root 	{ background-color:#ffffff; color:#1a1715; font-weight:bold; }',
			  'a 							{ background-color:#ffffff; color:#1a1715; font-weight:bold; text-decoration:underline; } ',
			  'a:hover 				{ background-color:#ffffff; color:#1a1715; text-decoration:none; } '		  
			  ]
		,wmode:'transparent'
		,filters: {
        DropShadow: {
           distance: 1
          ,color: '#000000'
          ,strength: 1
          ,alpha: .25
        }
      }

	});
	
	sIFR.replace(helvetica, {
		selector:'.line-feature h2',
		css:['.sIFR-root 	{ background-color:#ffffff; color:#4f658c; }',
			  'a 							{ background-color:#ffffff; color:#4f658c; text-decoration:underline; } ',
			  'a:hover 				{ background-color:#ffffff; color:#4f658c; text-decoration:none; } '		  
			  ]
		,wmode:'transparent'
		,forceWidth:true
		,fitExactly:true
	});
	
	sIFR.replace(helvetica, {
		selector:'.content h2, .content h3',
		css:['.sIFR-root 	{ background-color:#ffffff; color:#4f658c; }',
			  'a 							{ background-color:#ffffff; color:#4f658c; text-decoration:underline; } ',
			  'a:hover 				{ background-color:#ffffff; color:#4f658c; text-decoration:none; } '		  
			  ]
		,wmode:'transparent'
	});
	
	sIFR.replace(palatino, {
		selector:'.content blockquote p',
		css:['.sIFR-root 	{ background-color:#ffffff; color:#535150; font-style:italic; }',
			  'a 							{ background-color:#ffffff; color:#535150; text-decoration:underline; } ',
			  'a:hover 				{ background-color:#ffffff; color:#535150; text-decoration:none; } '		  
			  ]
		,wmode:'transparent'
	});
	