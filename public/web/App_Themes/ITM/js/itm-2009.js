$(document).ready(function(){		
		if($('.zebra-table').length) {
			$("tr:nth-child(even)").addClass("alt");
	 	}
		if($('.overlay').length) {
			$('.overlay').each(function()	{ setFocusValues($(this)); });
		}	
		if($('.row-scroll .scroll').length) {	
			$('.row-scroll .scroll').scrollable(	{size:5, items:'.scroll-items', clickable:false, loop:true} );			
		}		
		if($('.row-img-scroll .scroll').length) {				
			$('.row-img-scroll .scroll').scrollable({size:1, items:'.scroll-items', clickable:false, loop:true, interval: 6000, speed:600,
					onBeforeSeek: function() { this.getItems().fadeTo(300, 0.5); }, 
					onSeek: function() { this.getItems().fadeTo(300, 1);} 
				});			
		}	
		if($(".news-ticker").length){
			$(".news-ticker").newsticker(5000);
		}
	 
});
function setFocusValues(input) {
	input.focus(function(){ if ($(this).val() == '') {$(this).prev().addClass('focus');	}});	
	input.blur(function()	{ if ($(this).val() == '') { $(this).prev().removeClass('focus').removeClass('hastext');}});	
	if ($(input).val() != '') {$(input).prev().addClass('hastext');};	
	input.keydown(function(){ if ($(this).val() != '') {$(this).prev().addClass('hastext');}});	
}

function nextImg() {
	$('.linder-gallery-images').fadeOut('slow', swapImage);
}